import dolfinx as dfx
from mpi4py import MPI
import ufl
import numpy as np
from petsc4py.PETSc import ScalarType
import matplotlib.pyplot as plt
import dolfinx.fem.petsc

# -------------------------
# Parameters
# -------------------------
# Geometry paramaters
l_x, l_y = 1.0, 1.0  # Width, Height

# Number of elements
nx, ny = 10, 10

# Degree orders
deg = 1  # Order of elements

# Material parameters
E, nu = 210e9, 0.4  # Young's module, Poisson ratio
mu, lmbda = 0.5 * E / (1 + nu), E * nu / (1 + nu) / (1 - 2 * nu)  # Lame's coefficients

# --------------------
# Define geometry
# --------------------
mesh = dfx.mesh.create_rectangle(MPI.COMM_WORLD, [[0.0, 0.0], [l_x, l_y]], [nx, ny])
tdim = mesh.topology.dim
fdim = tdim - 1

# -------------------------
# Function space
# -------------------------
# Main space for displacement field
v = dfx.fem.FunctionSpace(mesh, ("P", 1, (2,)))

# Displacement functions
u_tr = ufl.TrialFunction(v)
u_test = ufl.TestFunction(v)
x_sol = dfx.fem.Function(v)

# -------------------------
# Geometry parts
# -------------------------
def left(x):
    return np.isclose(x[0], 0.0)

def right(x):
    return np.isclose(x[0], l_x)

def bottom(x):
    return np.isclose(x[1], 0.0)

def top(x):
    return np.isclose(x[1], l_y)

# -------------------------
# Boundary conditions
# -------------------------
def dirichletbc_subspace(subspace, location, value_i):
    v_i, submap_i = subspace.collapse()
    dofs = dfx.fem.locate_dofs_geometrical((subspace, v_i), location)
    return dfx.fem.dirichletbc(ScalarType(value_i), dofs[0], subspace)

bc1 = dirichletbc_subspace(v.sub(0), bottom, 0.0)
bc2 = dirichletbc_subspace(v.sub(1), bottom, 0.0)
bc3 = dirichletbc_subspace(v.sub(1), top, -0.5)
bc = [bc1, bc2, bc3]

# -------------------------
# Weak form
# -------------------------
# Elastic stress tensor
def sigma(u_i):
    return lmbda * (ufl.tr(eps(u_i))) * ufl.Identity(2) + 2.0 * mu * (eps(u_i))

# Elastic strain tensor
def eps(v_i):
    return ufl.sym(ufl.grad(v_i))

f = dfx.fem.Constant(mesh, np.array([0.0, 0.0]))
a = ufl.inner(sigma(u_tr), eps(u_test))*ufl.dx
L = ufl.inner(f, u_test) * ufl.dx

# -------------------------
# Solution
# -------------------------
u_sol = dfx.fem.Function(v)

problem = dfx.fem.petsc.LinearProblem(
    a, L, u=u_sol, bcs=bc, petsc_options={"ksp_type": "preonly", "pc_type": "lu"}
)
problem.solve()

vtk = dfx.io.VTKFile(mesh.comm, "linear_elasticity.pvd", "w")
vtk.write_function(u_sol, 0)
vtk.close()

import pyvista

#if pyvista.OFF_SCREEN:
#    pyvista.start_xvfb()
topology, cell_types, geometry = dfx.plot.vtk_mesh(mesh, tdim)
grid = pyvista.UnstructuredGrid(topology, cell_types, geometry)

plotter = pyvista.Plotter()
plotter.add_mesh(grid, show_edges=True)
plotter.view_xy()
if not pyvista.OFF_SCREEN:
    plotter.show()
else:
    plotter.show()

beta_3D = np.zeros((geometry.shape[0], 3))
beta_3D[:, :2] = u_sol.x.array.reshape(-1, 2)
grid["beta"] = beta_3D
grid.set_active_vectors("beta")

plotter = pyvista.Plotter()
#plotter.add_mesh(grid, show_edges=True)
# Attach vector values to grid and warp grid by vector
#grid["u"] = u_sol.x.array.reshape((geometry.shape[0], 2))
#actor_0 = plotter.add_mesh(grid, style="wireframe", color="k")
warped = grid.warp_by_vector("beta", factor=0.5)
actor_1 = plotter.add_mesh(warped, show_edges=True)
#p.show_axes()
plotter.view_xy()

if not pyvista.OFF_SCREEN:
    plotter.show()
else:
    #figure_as_array = p.screenshot("deflection.png")
    plotter.show()







#pyvista.start_xvfb()

# Create plotter and pyvista grid
#p = pyvista.Plotter()
if pyvista.OFF_SCREEN:
    pyvista.start_xvfb()
topology, cell_types, geometry = dfx.plot.vtk_mesh(mesh, tdim)
grid = pyvista.UnstructuredGrid(topology, cell_types, geometry)

p = pyvista.Plotter()
beta_3D = np.zeros((geometry.shape[0], 3))
beta_3D[:, :2] = u_sol.x.array.reshape(-1, 2) @ np.array([[0, -1], [1, 0]])
beta_3D[:, :2] = u_sol.x.array.reshape(-1, 2)
grid["beta"] = beta_3D
grid.set_active_vectors("beta")

# Attach vector values to grid and warp grid by vector
grid["u"] = u_sol.x.array.reshape((geometry.shape[0], 2))
actor_0 = p.add_mesh(grid, style="wireframe", color="k")
warped = grid.warp_by_vector("beta", factor=0.5)
actor_1 = p.add_mesh(warped, show_edges=True)
p.show_axes()
p.view_xy()
plotter.add_mesh(grid, show_edges=True)
plotter.view_xy()
if not pyvista.OFF_SCREEN:
    p.show()
else:
    #figure_as_array = p.screenshot("deflection.png")
    p.show()
