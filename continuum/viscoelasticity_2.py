import dolfinx as dfx
import matplotlib.pyplot as plt
import math
from mpi4py import MPI
import pyvista
import ufl
import numpy as np
from petsc4py.PETSc import ScalarType
import dolfinx.fem.petsc as dfp


# --------------------
# Classes and methods
# --------------------
def local_project(u_i, V_i):
	ut = ufl.TrialFunction(V_i)
	v_i = ufl.TestFunction(V_i)
	uh = dfx.fem.Function(V_i)
	problem = dfp.LinearProblem(
		ufl.inner(ut, v_i) * ufl.dx, ufl.inner(u_i, v_i) * ufl.dx, u=uh, bcs=[],
		petsc_options={"ksp_type": "preonly", "pc_type": "lu"}
	)
	problem.solve()
	return uh


def top(x):
	return np.isclose(x[2], 1.0)


def bottom(x):
	return np.isclose(x[2], 0.0)


def epsilon(a):
	return ufl.sym(ufl.grad(a))


def sigma(a):
	return lam*ufl.tr(epsilon(a))*ufl.Identity(d) + 2.0*mu*epsilon(a)


def sigma_ten(a):
	return local_project(lam*ufl.tr(ufl.grad(a))*ufl.Identity(d) + 2.0*mu*epsilon(a), ten)


# --------------------
# Define geometry
# --------------------
# mesh = UnitCubeMesh(5, 5, 5)
#mesh = fe.UnitCubeMesh.create(5,5,5,fe.CellType.Type.hexahedron)
mesh = dfx.mesh.create_unit_cube(MPI.COMM_WORLD, 10, 10, 10, dfx.mesh.CellType.hexahedron)
tdim = mesh.topology.dim
fdim = tdim - 1

# Hexahedrons do not support plotting
# plot(mesh, "Mesh")
# plt.show()

# --------------------
# Define spaces
# --------------------
#W = fe.VectorFunctionSpace(mesh, 'CG', 1)
#Ten = fe.TensorFunctionSpace(mesh, 'DG', 0)
#v = dfx.fem.FunctionSpace(mesh, ("P", 1, ))
w = dfx.fem.FunctionSpace(mesh, ("P", 1, (3,)))
ten = dfx.fem.FunctionSpace(mesh, ("P", 1, (3,3)))

dd_u = ufl.TrialFunction(w)
v = ufl.TestFunction(w)


# --------------------
# Parameters
# --------------------
start_time = 0.0
end_time = 30.0
tau = 1.0e-1  # time increment

# Dimension of task
d = 3

# Material parameters
n = 22
Ginf = 682.18e3
Gs = 1.0e3 * np.array(
	[6933.9, 3898.6, 2289.2, 1672.7, 761.6, 2401.0, 65.2, 248.0, 575.6, 56.3, 188.6, 445.1, 300.1, 401.6, 348.1,
	 111.6, 127.2, 137.8, 50.5, 322.9, 100.0, 199.9])
Ts = np.array(
	[1.0e-9, 1.0e-8, 1.0e-7, 1.0e-6, 1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5,
	 1.0e6, 1.0e7, 1.0e8, 1.0e9, 1.0e10, 1.0e11, 1.0e12])
eta = np.zeros(n)
for k in range(0, n):
	eta[k] = Ts[k]*Gs[k]

rho = 1.0e6
nu = 0.2

G_unit = 1.0
lam, mu = (2 * G_unit * nu / (1 - 2 * nu), G_unit)

# Other constants
A = []
B = []
C = []
sumA = 0.0
sumB = 0.0
sumC = 0.0

for k in range(0, n):
	A.append(math.exp(-Gs[k] / eta[k] * tau))
	sumA += A[k]
	B.append(-eta[k] * math.expm1(-Gs[k] / eta[k] * tau))
	sumB += B[k]
	C.append(0.5 * eta[k] * (tau + eta[k] / Gs[k] * math.expm1(-Gs[k] / eta[k] * tau)))
	sumC += C[k]

# --------------------
# Boundary conditions
# --------------------
boundaries = [(11, lambda x: np.isclose(x[2], 0)),
			  (22, lambda x: np.isclose(x[2], 1))]

facet_indices, facet_markers = [], []
fdim = mesh.topology.dim - 1
for (marker, locator) in boundaries:
	facets = dfx.mesh.locate_entities(mesh, fdim, locator)
	facet_indices.append(facets)
	facet_markers.append(np.full_like(facets, marker))
facet_indices = np.hstack(facet_indices).astype(np.int32)
facet_markers = np.hstack(facet_markers).astype(np.int32)
sorted_facets = np.argsort(facet_indices)
facet_tag = dfx.mesh.meshtags(mesh, fdim, facet_indices[sorted_facets], facet_markers[sorted_facets])

mesh.topology.create_connectivity(mesh.topology.dim-1, mesh.topology.dim)
with dfx.io.XDMFFile(mesh.comm, "facet_tags.xdmf", "w") as xdmf:
	xdmf.write_mesh(mesh)
	xdmf.write_meshtags(facet_tag, mesh.geometry)

ds = ufl.Measure("ds", domain=mesh, subdomain_data=facet_tag)

# Dirichlet boundary condition for bottom
bottom_dofs = dfx.fem.locate_dofs_geometrical(w, bottom)
u_bot = dfx.fem.Function(w)
bcs = [dfx.fem.dirichletbc(u_bot, bottom_dofs)]
#DB_bottom = fe.DirichletBC(W, fe.Constant((0.0, 0.0, 0.0)), bottom)

# --------------------
# Initialization
# --------------------
u = dfx.fem.Function(w)
d_u = dfx.fem.Function(w)
dd_u_ = dfx.fem.Function(w)
dd_u_old = dfx.fem.Function(w)
f_v = [dfx.fem.Function(ten) for i in range(n)]

t = start_time

#ff = fe.Expression(("0.0", "1.0e6", "0.0"), degree=0)
ff = dfx.fem.Constant(mesh, ScalarType([0.0, 1.0e6, 0.0]))

# --------------------
# XDMF output
# --------------------
# Create XDMF files for visualization output
out_file = "test2.xdmf"
with dfx.io.XDMFFile(mesh.comm, out_file, "w") as xdmf:
	xdmf.write_mesh(mesh)

# --------------------
# Initial condition
# --------------------
LHS0 = rho*ufl.dot(v, dd_u)*ufl.dx
RHS0 = ufl.dot(v, ff)*ds(1)
#fe.solve(LHS0 == RHS0, dd_u_old, [DB_bottom])
problem = dfx.fem.petsc.LinearProblem(
	LHS0, RHS0, u=dd_u_old, bcs=bcs, petsc_options={"ksp_type": "preonly", "pc_type": "lu"}
)
problem.solve()

# --------------------
# Variational form
# --------------------
LHS = rho*ufl.dot(v, dd_u)*ufl.dx + ufl.inner(epsilon(v), (0.25*Ginf*tau*tau + sumC)*sigma(dd_u))*ufl.dx
RHS = -ufl.inner(epsilon(v), Ginf*sigma(u))*ufl.dx - ufl.inner(epsilon(v), (Ginf*tau + sumB)*sigma(d_u))*ufl.dx - \
		ufl.inner(epsilon(v), (0.25*Ginf*tau*tau + sumC)*sigma(dd_u_old))*ufl.dx + ufl.dot(v, ff) * ds(22)
RHS -= ufl.inner(epsilon(v), sum(A[k]*f_v[k] for k in range(0, n)))*ufl.dx

temp1 = dfx.fem.Function(ten)
temp2 = dfx.fem.Function(ten)
temp3 = dfx.fem.Function(ten)
# --------------------
# Main load loop
# --------------------
while t <= end_time:
	print(t)
	# Solve linear variational problem
	#fe.solve(LHS == RHS, dd_u_, [DB_bottom])
	problem = dfx.fem.petsc.LinearProblem(
		LHS, RHS, u=dd_u_, bcs=bcs, petsc_options={"ksp_type": "preonly", "pc_type": "lu"}
	)
	problem.solve()

	# Update of rest quantities
	#temp1 = sigma_ten(d_u)
	#print(ufl.form(sigma(d_u)))
	temp1_exp = dfx.fem.Expression(sigma(d_u), ten.element.interpolation_points())
	temp1.interpolate(temp1_exp)
	#temp2 = sigma_ten(dd_u_)
	temp2_exp = dfx.fem.Expression(sigma(dd_u_), ten.element.interpolation_points())
	temp2.interpolate(temp2_exp)
	#temp3 = sigma_ten(dd_u_old)
	temp3_exp = dfx.fem.Expression(sigma(dd_u_old), ten.element.interpolation_points())
	temp3.interpolate(temp3_exp)
	for k in range(0, n):
		#f_v[k].assign(A[k]*f_v[k] + B[k]*temp1 + C[k]*temp2 + C[k]*temp3)
		f_v[k].x.array[:] = A[k]*f_v[k].x.array + B[k]*temp1.x.array + C[k]*temp2.x.array + C[k]*temp3.x.array

	#u.assign(u + d_u*tau + 0.25*(dd_u_old + dd_u_)*tau*tau)
	u.x.array[:] = u.x.array + d_u.x.array*tau + 0.25*(dd_u_old.x.array + dd_u_.x.array)*tau*tau
	#d_u.assign(d_u + 0.5 * (dd_u_old + dd_u_) * tau)
	d_u.x.array[:] = d_u.x.array + 0.5 * (dd_u_old.x.array + dd_u_.x.array) * tau
	#dd_u_old.assign(dd_u_)
	dd_u_old.x.array[:] = dd_u_.x.array

	# Save solution to file (XDMF/HDF5)
	with dfx.io.XDMFFile(mesh.comm, out_file, "a") as xdmf:
		xdmf.write_function(u, t)

	# Increment of time
	t = t + tau
