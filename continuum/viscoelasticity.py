import dolfinx as dfx
import matplotlib.pyplot as plt
import math
from mpi4py import MPI
import pyvista
import ufl
import numpy as np
from petsc4py.PETSc import ScalarType
import dolfinx.fem.petsc as dfp


# L2 projection onto the space V:
def my_project(u_i,V_i):
    ut = ufl.TrialFunction(V_i)
    v_i = ufl.TestFunction(V_i)
    uh = dfx.fem.Function(V_i)
    form_i = ufl.inner(ut,v_i)*ufl.dx==ufl.inner(u_i,v_i)*ufl.dx
    problem = dfp.LinearProblem(
        ufl.inner(ut, v_i)*ufl.dx, ufl.inner(u_i, v_i)*ufl.dx, u=uh, bcs=[], petsc_options={"ksp_type": "preonly", "pc_type": "lu"}
    )
    problem.solve()
    return uh


# --------------------
# Parameters
# --------------------
start_time = 0.0
end_time = 8.0
tau = 0.05  # time increment

# Dimension of task
d = 3

# Material parameters
G_inf = 1.0
G_1 = 1.0
eta_1 = 0.5
rho = 2.0
nu = 0.49

G_unit = 1.0
lam, mu = (2 * G_unit * nu / (1 - 2 * nu), G_unit)

# Other constants
A = math.exp(-G_1 / eta_1 * tau)
B = eta_1 * (1.0 - A)
C = 0.5 * eta_1 * (tau - B / G_1)

# --------------------
# Define geometry
# --------------------
mesh = dfx.mesh.create_unit_cube(MPI.COMM_WORLD, 10, 10, 10)
tdim = mesh.topology.dim
fdim = tdim - 1

topology, cell_types, geometry = dfx.plot.vtk_mesh(mesh, tdim)
grid = pyvista.UnstructuredGrid(topology, cell_types, geometry)

plotter = pyvista.Plotter()
plotter.add_mesh(grid, show_edges=True)
plotter.view_xy()
if not pyvista.OFF_SCREEN:
    plotter.show()
else:
    plotter.show()

# --------------------
# Define spaces
# --------------------
v = dfx.fem.FunctionSpace(mesh, ("P", 1, ))
w = dfx.fem.FunctionSpace(mesh, ("P", 1, (3,)))
ten = dfx.fem.FunctionSpace(mesh, ("P", 1, (3,3)))
s = ufl.TrialFunction(v)
q = ufl.TestFunction(v)
u = ufl.TrialFunction(w)
v = ufl.TestFunction(w)

# --------------------
# Boundary conditions
# --------------------
boundaries = [(11, lambda x: np.isclose(x[2], 0)),
              (22, lambda x: np.isclose(x[2], 1))]

facet_indices, facet_markers = [], []
fdim = mesh.topology.dim - 1
for (marker, locator) in boundaries:
    facets = dfx.mesh.locate_entities(mesh, fdim, locator)
    facet_indices.append(facets)
    facet_markers.append(np.full_like(facets, marker))
facet_indices = np.hstack(facet_indices).astype(np.int32)
facet_markers = np.hstack(facet_markers).astype(np.int32)
sorted_facets = np.argsort(facet_indices)
facet_tag = dfx.mesh.meshtags(mesh, fdim, facet_indices[sorted_facets], facet_markers[sorted_facets])

mesh.topology.create_connectivity(mesh.topology.dim-1, mesh.topology.dim)
with dfx.io.XDMFFile(mesh.comm, "facet_tags.xdmf", "w") as xdmf:
    xdmf.write_mesh(mesh)
    xdmf.write_meshtags(facet_tag, mesh.geometry)

ds = ufl.Measure("ds", domain=mesh, subdomain_data=facet_tag)


def bottom(x):
    return np.isclose(x[2], 0.0)


bottom_dofs = dfx.fem.locate_dofs_geometrical(w, bottom)
u_bot = dfx.fem.Function(w)
bcs = [dfx.fem.dirichletbc(u_bot, bottom_dofs)]

# --------------------
# Methods
# --------------------
def epsilon(a):
    return ufl.sym(ufl.grad(a))


def sigma(a):
    return lam * ufl.tr(epsilon(a)) * ufl.Identity(d) + 2.0 * mu * epsilon(a)


# --------------------
# Initialization
# --------------------
u_old = dfx.fem.Function(w)
u_ = dfx.fem.Function(w)

d_u = dfx.fem.Function(w)
dd_u = dfx.fem.Function(w)
dd_u_old = dfx.fem.Function(w)
u_bar = dfx.fem.Function(w)
f_v = dfx.fem.Function(ten)

t = start_time
ff = dfx.fem.Constant(mesh, ScalarType([0.0, 1.0, 0.0]))

# --------------------
# XDMF output
# --------------------
out_file = "test.xdmf"
with dfx.io.XDMFFile(mesh.comm, out_file, "w") as xdmf:
    xdmf.write_mesh(mesh)
# --------------------
# Main load loop
# --------------------
while t <= end_time:
    print(t)

    # u_bar update
    u_bar.x.array[:] = u_old.x.array + d_u.x.array*tau + 0.25*dd_u.x.array*tau*tau

    # Variational problem
    E_du = (ufl.inner(G_inf * epsilon(u) + A * f_v + B * epsilon(d_u) + C * epsilon(dd_u) + C * 4 / (tau * tau) * (
                epsilon(u) - epsilon(u_bar)), sigma(v)) \
            + 4 * rho / (tau * tau) * (ufl.inner(u, v) - ufl.inner(u_bar, v))) * ufl.dx \
           + ufl.inner(u_bot, v) * ufl.dx \
           - ufl.inner(ff, v) * ds(22)

    E_du_lhs = ufl.lhs(E_du)
    E_du_rhs = ufl.rhs(E_du)
    problem = dfx.fem.petsc.LinearProblem(
        E_du_lhs, E_du_rhs, u=u_, bcs=bcs, petsc_options={"ksp_type": "preonly", "pc_type": "lu"}
    )
    problem.solve()

    # Update of rest quantities
    f_v = my_project(A * f_v + B * epsilon(d_u) + C * epsilon(dd_u) + C * 4 / (tau * tau) * (epsilon(u_) - epsilon(u_bar)),
                  ten)
    u_old.x.array[:] = u_.x.array
    dd_u_old.x.array[:] = dd_u.x.array
    dd_u.x.array[:] = 4 / (tau * tau) * (u_.x.array - u_bar.x.array)
    d_u.x.array[:] = d_u.x.array + 0.5 * (dd_u_old.x.array + dd_u.x.array) * tau

    # Save solution to file (XDMF/HDF5)
    with dfx.io.XDMFFile(mesh.comm, out_file, "a") as xdmf:
        xdmf.write_function(u_, t)

    # Increment of time
    t = t + tau
