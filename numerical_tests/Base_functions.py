# -------------------------
# Base_functions.py
# -------------------------

# -------------------------
# Description:
# - Plotting of shape of base functions
# - Plotting of gauss points
#
# Last edit: 13.02. 2024
# CLEANED
# -------------------------

import basix
import matplotlib.pyplot as plt
import numpy as np
from basix import CellType, QuadratureType
from mpi4py import MPI


# Plot base function across one 1D interval element
def plot_base_functions(deg, type_i):
    if type_i == "H":
        element = basix.create_element(basix.ElementFamily.Hermite, basix.CellType.interval, deg),
    else:
        element = basix.create_element(basix.ElementFamily.P, basix.CellType.interval, deg,
                                       basix.LagrangeVariant.equispaced)
    pts = basix.create_lattice(basix.CellType.interval, 200, basix.LatticeType.equispaced, True)
    values = element.tabulate(0, pts)[0, :, :, 0]
    if MPI.COMM_WORLD.size == 1:  # Skip this plotting in parallel
        for i in range(values.shape[1]):
            plt.plot(pts, values[:, i])
        plt.plot(element.points, [0 for _ in element.points], "ko")
    plt.autoscale()
    plt.show()


# Plot Gauss points for 2D element
def plot_quad_points_2d(cell_type, deg, color, ax):
    points, weights = basix.make_quadrature(cell_type, deg)

    vertices = basix.geometry(cell_type)
    facets = basix.cell.sub_entity_connectivity(cell_type)[1]

    for f in facets:
        vert = vertices[f[0], :]
        ax.plot(vert[:, 0], vert[:, 1], "k")

    ax.scatter(points[:, 0], points[:, 1], 500 * weights, color=color)
    ax.set_aspect("equal")


def plot_quad_points_nd(cell_type, deg, color, ax):
    points, weights = basix.make_quadrature(cell_type, deg)

    vertices = basix.geometry(cell_type)
    facets = basix.cell.sub_entity_connectivity(cell_type)[1]

    if cell_type != CellType.interval:
        for f in facets:
            vert = vertices[f[0], :]
            ax.plot(vert[:, 0], vert[:, 1], "k")
        ax.scatter(points[:, 0], points[:, 1], 500 * weights, color=color)
        ax.set_aspect("equal")
    else:
        ax.plot([0.0, 1.0], [0.0, 0.0], "k")
        pointsz = np.zeros(len(points))
        ax.scatter(points[:, 0], pointsz, 500 * weights, color=color)
        ax.set_aspect("equal")


# Plot Gauss points for 1D element
def plot_quad_points_1d(cell_type, deg, color, ax):
    points, weights = basix.make_quadrature(cell_type, deg)
    pointsz = np.zeros(len(points))

    vertices = basix.geometry(cell_type)
    facets = basix.cell.sub_entity_connectivity(cell_type)[1]

    for f in facets:
        vert = vertices[f[0], :]
        print(vert[:, 0])
        ax.plot(vert[:, 0], np.zeros(len(vert[:, 0])), "k")

    ax.scatter(points[:, 0], pointsz, 500 * weights, color=color)
    ax.set_aspect("equal")


# Plot Gauss points based on quadrature rule for 2D element
def plot_quadrature_rule_2d(ctype, deg, color="C0"):
    plt.figure()
    ax = plt.gca()
    ax.margins(0.05)

    plot_quad_points_nd(ctype, deg, color, ax)
    plt.title(f"{ctype} element type, degree $d={1}$")
    plt.show()


# Plot Gauss points based on quadrature rule for 2D element
def plot_quadrature_rule_nd(ctype, deg, color="C0"):
    plt.figure()
    ax = plt.gca()
    ax.margins(0.05)

    plot_quad_points_nd(ctype, deg, color, ax)
    plt.title(f"{ctype} element type, degree $d={1}$")
    plt.show()


# Plot Gauss points based on quadrature rule for 1D element
def plot_quadrature_rule_1d(ctype, deg, color="C0"):
    plt.figure()
    ax = plt.gca()
    ax.margins(0.05)

    plot_quad_points_nd(ctype, deg, color, ax)
    plt.show()


all_cell_types = [CellType.triangle, CellType.quadrilateral]
degrees = range(5)
quad_rules = {
    QuadratureType.Default: (degrees, all_cell_types),
    QuadratureType.gauss_jacobi: (degrees, all_cell_types),
    QuadratureType.gll: (degrees, [CellType.quadrilateral]),
    QuadratureType.xiao_gimbutas: (degrees[1:], [CellType.triangle]),
}

plot_quadrature_rule_1d(CellType.interval, 2, color="C3")
#plot_quadrature_rule_1d(CellType.interval, 40, color="C3")
#plot_quadrature_rule_2d(CellType.triangle, 1, color="C3")
#plot_quadrature_rule_2d(CellType.triangle, 4, color="C3")
plot_quadrature_rule_2d(CellType.quadrilateral, 2, color="C3")

plot_base_functions(1, "L")
plot_base_functions(2, "L")
plot_base_functions(3, "Bubble")
