# -------------------------
# Base_functions.py
# -------------------------

# -------------------------
# Description:
# - Plotting of shape of base functions
# - Plotting of gauss points
#
# Last edit: 13.02. 2024
# CLEANED
# -------------------------

import basix
import matplotlib.pyplot as plt
import numpy as np
from basix import CellType, QuadratureType
from mpi4py import MPI


vtk = dfx.io.VTKFile(domain.comm, "linear_elasticity.pvd", "w")
vtk.write_function(u_sol)