# -------------------------
# Assembling_of_matrices.py
# -------------------------

# -------------------------
# Description:
# - Assembling of matrices
#
# Last edit: 13.02. 2024
# CLEANED
# -------------------------

from mpi4py import MPI
import dolfinx as dfx
import numpy as np
import ufl
from dolfinx import default_scalar_type
import matplotlib.pyplot as plt

print(dfx.__version__)


def plot_and_print_matrices(mx, type):
    # --------------------
    # Geometry
    # --------------------
    if type == "int":
        domain = dfx.mesh.create_unit_interval(MPI.COMM_WORLD, mx)
    elif type == "rect":
        domain = dfx.mesh.create_unit_square(MPI.COMM_WORLD, mx, mx, dfx.mesh.CellType.quadrilateral)
    else:
        domain = dfx.mesh.create_unit_square(MPI.COMM_WORLD, mx, mx, dfx.mesh.CellType.triangle)
    tdim = domain.topology.dim
    fdim = tdim - 1

    import pyvista
    if pyvista.OFF_SCREEN:
        pyvista.start_xvfb()
    topology, cell_types, geometry = dfx.plot.vtk_mesh(domain, tdim)
    grid = pyvista.UnstructuredGrid(topology, cell_types, geometry)

    plotter = pyvista.Plotter()
    plotter.add_mesh(grid, show_edges=True)
    plotter.view_xy()
    if not pyvista.OFF_SCREEN:
        plotter.show()
    else:
        plotter.show()

    # --------------------
    # Function spaces
    # --------------------
    V = dfx.fem.FunctionSpace(domain, ("Lagrange", 1))

    # --------------------
    # Boundary conditions
    # --------------------
    uD = dfx.fem.Function(V)
    uD.interpolate(lambda x: 1000 + 0*x[0])
    # Create facet to cell connectivity required to determine boundary facets
    domain.topology.create_connectivity(fdim, tdim)
    boundary_facets = dfx.mesh.exterior_facet_indices(domain.topology)
    boundary_dofs = dfx.fem.locate_dofs_topological(V, fdim, boundary_facets)
    bc = dfx.fem.dirichletbc(uD, boundary_dofs)

    # --------------------
    # Weak form
    # --------------------
    u = ufl.TrialFunction(V)
    v = ufl.TestFunction(V)

    f = dfx.fem.Constant(domain, default_scalar_type(-6))
    a = ufl.dot(ufl.grad(u), ufl.grad(v)) * ufl.dx
    L = f * v * ufl.dx

    # --------------------
    # Matrices
    # --------------------
    a_matrix = dfx.fem.assemble_matrix(dfx.fem.form(a))
    #l_vector = dfx.fem.assemble_matrix(dfx.fem.form(L))

    # Printing matrices
    print("A matrix:\n", np.array_str(a_matrix.to_dense(), precision=3))

    # Plotting matrices
    #matrix = a_matrix.to_scipy().toarray()
    matrix = a_matrix.to_dense()
    m_inv = np.linalg.inv(matrix[1:, 1:])

    # Printing matrices
    print("A inverse:\n", np.array_str(m_inv, precision=3))
    plt.spy(matrix)
    plt.show()
    plt.spy(m_inv)
    plt.show()


plot_and_print_matrices(2, "int")
plot_and_print_matrices(10, "int")
plot_and_print_matrices(100, "int")
plot_and_print_matrices(2, "rect")
plot_and_print_matrices(10, "rect")
plot_and_print_matrices(2, "tri")
plot_and_print_matrices(10, "tri")
