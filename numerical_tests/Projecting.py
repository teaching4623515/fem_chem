# -------------------------
# Projecting.py
# -------------------------

# -------------------------
# Description:
# - Projecting function on P1 space
#
# Last edit: 13.02. 2024
# CLEANED
# -------------------------

from mpi4py import MPI
import dolfinx as dfx
import numpy as np
import ufl
from dolfinx import default_scalar_type
import matplotlib.pyplot as plt
import dolfinx.fem.petsc as dfp
from petsc4py.PETSc import ScalarType


# L2 projection onto the space V:
def my_project(u_i,V_i):
    ut = ufl.TrialFunction(V_i)
    v_i = ufl.TestFunction(V_i)
    uh = dfx.fem.Function(V_i)
    form_i = ufl.inner(ut,v_i)*ufl.dx==ufl.inner(u_i,v_i)*ufl.dx
    problem = dfp.LinearProblem(
        ufl.inner(ut, v_i)*ufl.dx, ufl.inner(u_i, v_i)*ufl.dx, u=uh, bcs=[], petsc_options={"ksp_type": "preonly", "pc_type": "lu"}
    )
    problem.solve()
    return uh


def plot_projected_fce(mx):
    # --------------------
    # Geometry
    # --------------------
    domain = dfx.mesh.create_unit_interval(MPI.COMM_WORLD, mx)
    tdim = domain.topology.dim
    fdim = tdim - 1

    # --------------------
    # Function spaces
    # --------------------
    V = dfx.fem.FunctionSpace(domain, ("Lagrange", 1))
    x = V.tabulate_dof_coordinates()
    xs = ufl.SpatialCoordinate(domain)
    x_order = np.argsort(x[:, 0])

    # --------------------
    # Weak form
    # --------------------
    u = ufl.TrialFunction(V)
    v = ufl.TestFunction(V)

    # --------------------
    # Projecting vs. interpolation
    # --------------------
    def initial_condition(x):
        return 1.0 * (x[0] < 0.5)
        #return (x[0]-0.5)*(x[0]>0.5)
    w1 = dfx.fem.Function(V)
    w1.interpolate(initial_condition)
    w2 = my_project(ufl.conditional(xs[0] < 0.5, 1, 0), V)
    #w2 = my_project(ufl.conditional(xs[0] < 0.5, 0, xs[0]-0.5), V)

    plt.plot(x[x_order, 0], w1.x.array[x_order], label="Interpolation")
    plt.plot(x[x_order, 0], w2.x.array[x_order], "--", label="Projecting")
    plt.legend()
    plt.title("Projection vs. interpolation for mesh with {} elements".format(mx))
    plt.show()


plot_projected_fce(31)
